<?php

	namespace AppFog;

	class Configurator {

		public static function configure() {
			$options = getenv('VCAP_SERVICES');
			$options = json_decode($options);

			if( empty( $options ) )
				return;

			$key = 'mysql-5.1';
			if( !isset( $options->$key ) )
				return;
			
			self::_configureMysql($options->$key);

		}

		protected static function _configureMysql( $opt ) {
			if( empty( $opt ) )
				return;

			$credentials = array_pop( $opt );
			$credentials = $credentials->credentials;

			$prefix = \WebLab_Config::getApplicationConfig()->get('AppFog.MySQL.prefix', \WebLab_Config::RAW, false);
			$config = &\WebLab_Config::getApplicationConfig()->get('Application.Data.DB');
			$config['main'] = (object)array(
				'auto' => false,
				'type' => 'MySQLi',
				'username' => $credentials->username,
				'password' => $credentials->password,
				'host' => $credentials->hostname,
				'database' => $credentials->name,
				'port' => $credentials->port,
				'prefix' => empty( $prefix ) ? '' : $prefix
			);
		}

	}